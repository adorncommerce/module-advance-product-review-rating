var config = {
    map: {
        "*": {
            rateyo: 'Adorncommerce_ProductReviewRating/js/jquery.rateyo.min',
        }
    },
    shim: {
        rateyo: {
            deps: ['jquery']
        }
    }
};
