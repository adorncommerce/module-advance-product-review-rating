<?php
/**
 * Copyright 2020 Adorncommerce LLP. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Adorncommerce\ProductReviewRating\Plugin\Adminhtml\Add;


class Form extends \Magento\Review\Block\Adminhtml\Add\Form
{
    public function beforeSetForm(\Magento\Review\Block\Adminhtml\Add\Form $object, $form) {
        $review = $object->_coreRegistry->registry('review_data');
        $fieldset = $form->addFieldset(
            'review_details_extra',
            ['legend' => __(''), 'class' => 'fieldset-wide']
        );

        $fieldset->addField(
            'admin_reply',
            'text',
            ['label' => __('Admin Reply'), 'required' => false, 'name' => 'admin_reply']
        );

        return [$form];
    }
}
