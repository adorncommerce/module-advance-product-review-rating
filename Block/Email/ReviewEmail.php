<?php
/**
 * Copyright 2020 Adorncommerce LLP. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Adorncommerce\ProductReviewRating\Block\Email;


use Magento\Framework\View\Element\Template;
use Magento\Review\Model\ResourceModel\Review\Collection as ReviewCollection;

class ReviewEmail extends \Magento\Framework\View\Element\Template
{
    protected $_storeManager;
    protected $_ratingFactory;
    protected $_productFactory;
    protected $_reviewFactory;
    protected $_voteFactory;

    protected $_voteCollection = false;
    /**
     * Review collection
     *
     * @var ReviewCollection
     */
    protected $_reviewsCollection;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Review\Model\ResourceModel\Rating\CollectionFactory $ratingFactory,
        \Magento\Review\Model\ReviewFactory $reviewFactory,
        \Magento\Review\Model\Rating\Option\VoteFactory $voteFactory,
        array $data = [])
    {
        $this->_storeManager = $storeManager;
        $this->_productFactory = $productFactory;
        $this->_ratingFactory = $ratingFactory;
        $this->_reviewFactory = $reviewFactory;
        $this->_productFactory = $productFactory;
        $this->_voteFactory = $voteFactory;
        parent::__construct($context, $data);
    }

    public function getReviewsCollection($productId, $reviewId)
    {
        if (null === $this->_reviewsCollection) {
            $this->_reviewsCollection = $this->_reviewFactory->create()->load($reviewId);
        }
        return $this->_reviewsCollection;
    }
}
