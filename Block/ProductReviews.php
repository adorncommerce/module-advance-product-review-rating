<?php
/**
 * Copyright 2020 Adorncommerce LLP. All rights reserved.
 * See LICENSE.txt for license details.
 */
namespace Adorncommerce\ProductReviewRating\Block;

use \Magento\Framework\View\Element\Template;

class ProductReviews extends Template

{
    protected $_ratingFactory;
    protected $_productFactory;
    protected $_reviewFactory;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * ProductReviews constructor.
     * @param Template\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Review\Model\RatingFactory $ratingFactory
     * @param \Magento\Review\Model\ResourceModel\Review\CollectionFactory $reviewFactory
     * @param array $data
     */
    public function __construct(Template\Context $context, \Magento\Store\Model\StoreManagerInterface $storeManager,
                                \Magento\Catalog\Model\ProductFactory $productFactory,
                                \Magento\Review\Model\RatingFactory $ratingFactory,
                                \Magento\Review\Model\ResourceModel\Review\CollectionFactory $reviewFactory,
                                array $data = [])
    {
        $this->_storeManager = $storeManager;
        $this->_productFactory = $productFactory;
        $this->_ratingFactory = $ratingFactory;
        $this->_reviewFactory = $reviewFactory;
        parent::__construct($context, $data);
    }

    public function getReviewCollection($productId)
    {
        $collection = $this->_reviewFactory->create()
            ->addStatusFilter(
                \Magento\Review\Model\Review::STATUS_APPROVED
            )->addEntityFilter(
                'product',
                $productId
            )->setDateOrder();
    }

    public function getAllStart($pid) {
        $review = $this->_reviewFactory->create()     //\Magento\Review\Model\Review $reviewFactory (_objectReview)
        ->addFieldToFilter('main_table.status_id', 1)
            ->addEntityFilter('product', $pid)          //$pid = > your current product ID
            ->addStoreFilter($this->_storeManager->getStore()->getId())
            ->addFieldToSelect('review_id')
        ;
        $review->getSelect()->columns('detail.detail_id')->joinInner(
            ['vote' => $review->getTable('rating_option_vote')], 'main_table.review_id = vote.review_id', array('review_value' => 'vote.value')
        );
        $review->getSelect()->order('review_value DESC');
        $review->getSelect()->columns('count(vote.vote_id) as total_vote')->group('review_value');
        for ($i = 5; $i >= 1; $i--) {
            $arrRatings[$i]['value'] = 0;
        }
        foreach ($review as $_result) {
            $arrRatings[$_result['review_value']]['value'] = $_result['total_vote'];
        }
        return $arrRatings;
    }
}
