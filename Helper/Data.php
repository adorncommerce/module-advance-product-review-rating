<?php
/**
 * Copyright 2020 Adorncommerce LLP. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Adorncommerce\ProductReviewRating\Helper;


use Magento\Framework\App\Helper\Context;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $_reviewLikeFactory;
    protected $_customerSession;
    public function __construct(
        Context $context,
        \Adorncommerce\ProductReviewRating\Model\ReviewLikeFactory $reviewLikeFactory,
        \Magento\Customer\Model\Session $customerSession
    )
    {
        $this->_reviewLikeFactory = $reviewLikeFactory;
        $this->_customerSession = $customerSession;
        parent::__construct($context);
    }

    public  function isModuleEnabled(){
        return$this->scopeConfig->getValue('rating/general/enable');
    }

    public  function getColorCode(){
        return$this->scopeConfig->getValue('rating/general/star_color_option');
    }

    public  function getGraphicCircle(){
        return$this->scopeConfig->getValue('rating/general/graphic_circle');
    }

    public  function getRecipientEamil(){
        return $this->scopeConfig->getValue('rating/general/admin_identity');
    }

    public function getLikes($review_id){
        $reviewLikeFact =  $this->_reviewLikeFactory->create();
        return $reviewLikeFact->load($review_id, 'review_id');
    }

    public function getCustomerSession(){
        return $this->_customerSession;
    }

    public function getAdminReplyEmailTemplate(){
        return $this->scopeConfig->getValue('rating/general/admin_reply_email');
    }

    public function getAdminReciveEmailTemplate(){
        return $this->scopeConfig->getValue('rating/general/review_email');
    }
}
