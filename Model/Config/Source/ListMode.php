<?php
/**
 * Copyright 2020 Adorncommerce LLP. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Adorncommerce\ProductReviewRating\Model\Config\Source;


class ListMode implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        return [
            ['value' => 'star', 'label' => __('Star')],
            ['value' => 'round', 'label' => __('Circle')],
            ['value' => 'prograss', 'label' => __('Prograss Bar')]
        ];
    }
}
