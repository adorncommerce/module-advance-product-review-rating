<?php
/**
 * Copyright 2020 Adorncommerce LLP. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Adorncommerce\ProductReviewRating\Model\Observer;

use Magento\Framework\Event\ObserverInterface;

class ReviewSave implements ObserverInterface
{
    protected $_resource;
    protected $transportBuilder;
    protected $_storeManager;
    protected $scopeConfig;
    protected $_helperData;
    protected $_customerFactory;
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Adorncommerce\ProductReviewRating\Helper\Data $helperData,
        \Magento\Customer\Model\CustomerFactory $customerFactory
    )
    {
        $this->_resource = $resource;
        $this->transportBuilder = $transportBuilder;
        $this->_storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
        $this->_helperData = $helperData;
        $this->_customerFactory = $customerFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $currentAreacode = \Magento\Framework\App\ObjectManager::getInstance()->get('Magento\Framework\App\State')->getAreaCode();
        $review = $observer->getEvent()->getDataObject();
        if ($currentAreacode == 'adminhtml') {
            $connection = $this->_resource;

            $tableName = $connection->getTableName('review_detail');
            $detail = [
                'admin_reply' => $review->getAdminReply(),
            ];

            $select = $connection->getConnection()->select()->from($tableName)->where('review_id = :review_id');
            $detailId = $connection->getConnection()->fetchOne($select, [':review_id' => $review->getId()]);

            if ($detailId) {
                $condition = ["detail_id = ?" => $detailId];
                $connection->getConnection()->update($tableName, $detail, $condition);
            } else {
                $detail['store_id'] = $review->getStoreId();
                $detail['customer_id'] = $review->getCustomerId();
                $detail['review_id'] = $review->getId();
                $connection->getConnection()->insert($tableName, $detail);
            }
            if ($review->hasDataChanges()) {
                $oldStatus = $review->getOrigData('status_id');
                $newStatus = $review->getData('status_id');
                if ($newStatus != $oldStatus && $newStatus == \Magento\Review\Model\Review::STATUS_APPROVED) {
                    if ($review->getAdminReply() && $review->getCustomerId()) {
                        $customer = $this->_customerFactory->create()->load($review->getCustomerId());
                        $store = $this->_storeManager->getStore();
                        $templateVars['customer_name'] = $review['nickname'];
                        $templateVars['admin_reply'] = $review['admin_reply'];
                        $templateVars['title'] = $review['title'];
                        $templateVars['customer_review'] = $review['detail'];
                        $templateId = $this->_helperData->getAdminReplyEmailTemplate();
                        $from = $this->_helperData->getRecipientEamil();
                        $template = $this->transportBuilder->setTemplateIdentifier($templateId)
                            ->setTemplateOptions(['area' => 'frontend', 'store' => $store->getId()])
                            ->setTemplateVars($templateVars)
                            ->setFrom($from)
                            ->addTo($customer->getEmail(), $customer->getName())
                            ->getTransport();
                        $template->sendMessage();
                    }
                }
            }
        } else {
            $store = $this->_storeManager->getStore();
            $templateVars['customer_name'] = $review['nickname'];
            $templateVars['title'] = $review['title'];
            $templateVars['customer_review'] = $review['detail'];
            $templateVars['review_id'] = $review['review_id'];
            $templateVars['product_id'] = $review['entity_pk_value'];
            $templateId = $this->_helperData->getAdminReciveEmailTemplate();
            $from = $this->_helperData->getRecipientEamil();
            $email = $this->scopeConfig->getValue('trans_email/ident_support/email',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $name  = $this->scopeConfig->getValue('trans_email/ident_support/name',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $template = $this->transportBuilder->setTemplateIdentifier($templateId)
                ->setTemplateOptions(['area' => 'frontend', 'store' => $store->getId()])
                ->setTemplateVars($templateVars)
                ->setFrom($from)
                ->addTo($email, $name)
                ->getTransport();
           return $template->sendMessage();
        }
    }
}
