<?php
/**
 * Copyright 2020 Adorncommerce LLP. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Adorncommerce\ProductReviewRating\Model\ResourceModel\ReviewLink;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'likes_id';
    protected $_eventPrefix = 'review_rating_collection';
    protected $_eventObject = 'review_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Adorncommerce\ProductReviewRating\Model\ReviewLike', 'Adorncommerce\ProductReviewRating\Model\ResourceModel\ReviewLike');
    }

}
