<?php
/**
 * Copyright 2020 Adorncommerce LLP. All rights reserved.
 * See LICENSE.txt for license details.
 */

namespace Adorncommerce\ProductReviewRating\Model;


class ReviewLike extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'review_rating';

    protected $_cacheTag = 'review_rating';

    protected $_eventPrefix = 'review_rating';

    protected function _construct()
    {
        $this->_init('Adorncommerce\ProductReviewRating\Model\ResourceModel\ReviewLike');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}
